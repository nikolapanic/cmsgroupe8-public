// admin-tpl
var showpassword = false;
var is_user_modify = false;
$(document).ready(function() {
    let path = window.location.pathname;
    activeClass(path);
    $('.mobile-menu-btn').click(function(){
        toggleMenu();
    });
    $('.close-btn').click(function(){
        toggleMenu();
    });
    $('#show-password').click(function(){
        if(showpassword){
            $('input#pwd').attr('type','text');
        }else{
            $('input#pwd').attr('type','password');
        }
        showpassword = !showpassword;
    });
})

// voir plus... pour les commentaires
function readMore(i) {
    $(".show-read-more-"+i).addClass('display-none')
    $(".read-more-"+i).addClass('display-none')
    $(".full-comment-"+i).removeClass('display-none')
}


function activeClass(path){
    var href = '[href="' + path + '"]';
    $(href).parents('.menu-link').addClass('active');
}
$('[href="#"]').click(function(e) {
    e.preventDefault();
})

function toggleMenu(){
    var element = $('.menu');
    element.toggleClass('open');
}
// global
 
//create user
$(document).ready(function() {
    let path = window.location.pathname;
    if (path === '/user/create_user') {
        $('#formButton').addClass('justify-content-center');
    }
    //alert
    $('#close-message-button').click(function(){
        $('.card-message').css('display','none');
    }); 
});

//modify user
function userModify(id) {
    $("#user_modify"+id).toggleClass('display-none');
    $("#user"+id).toggleClass('display-none');
    $(".close-btn"+id).toggleClass('display-none');
    $(".default-btn."+id).toggleClass('display-none');
    if(is_user_modify){
        $(".default-btn").css('pointer-events','auto');
    }else{
        $(".default-btn").css('pointer-events','none'); 
    }
    is_user_modify=!is_user_modify;
}


function submenu() {
    var submenu = document.getElementById('sub-menu');
    var arrow = document.getElementById('arrow-submenu');

    if (submenu.classList.contains("show")) {
        submenu.classList.remove("show");
        arrow.innerHTML = '<span id="arrow-submenu" class="iconify" data-inline="false" data-icon="dashicons:arrow-down-alt2"></span>';
        
      } else {
        submenu.classList.add("show");
        arrow.innerHTML = '<span id="arrow-submenu" class="iconify" data-inline="false" data-icon="dashicons:arrow-up-alt2"></span>';
      }
}

// Ajouter une catégorie (page créer article)
function addCategorie() {
    var btn = document.getElementById('btn-add-categorie');
    btn.innerHTML = '<input type="text" name="categorie" id="add_categorie_input">';
}


// Afficher la preview (page créer articles)
function preview() {
    var bg_preview = document.getElementById('bg_preview');
    var preview = document.getElementById('preview_articles');
    bg_preview.style.display = 'block';
    preview.style.display = 'block';
    document.body.style.overflow = 'hidden';
    var title = document.getElementById('name_article').value;
    var title_preview = document.getElementById('title_preview');
    title_preview.innerHTML = title;
    var content= tinyMCE.get('mytextarea').getContent();
    var preview_content = document.getElementById('preview_content');
    preview_content.innerHTML = content;
}

// Ne plus voir la preview (page créer article)
function close_preview() {
    var bg_preview = document.getElementById('bg_preview');
    var preview = document.getElementById('preview_articles');
    bg_preview.style.display = 'none';
    preview.style.display = 'none';
    document.body.style.overflow = 'auto';
}