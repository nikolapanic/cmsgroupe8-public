-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : database
-- Généré le : sam. 10 juil. 2021 à 12:53
-- Version du serveur :  5.7.34
-- Version de PHP : 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `mvcdocker2`
--

-- --------------------------------------------------------

--
-- Structure de la table `wlms_articles`
--

CREATE TABLE `wlms_articles` (
  `id` int(11) NOT NULL,
  `title` varchar(70) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(30) DEFAULT NULL,
  `categorie` varchar(40) DEFAULT NULL,
  `Content` varchar(800) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `wlms_articles`
--

INSERT INTO `wlms_articles` (`id`, `title`, `date`, `author`, `categorie`, `Content`) VALUES
(8, 'test', '2021-07-10 09:54:41', 'toto toto', 'test', ''),
(9, 'TEST', '2021-07-10 10:15:34', 'hehe test', NULL, '<p>TEST</p>'),
(10, 'CACACACA', '2021-07-10 10:17:56', 'dfdsf gdfgdfg', 'cat2', '<p>dsfsqfd</p>'),
(13, 'sdqf', '2021-07-10 10:41:19', 'toto toto', 'oighsqdfg', '<p>sqdf</p>'),
(14, 'test', '2021-07-10 11:09:26', 'nikola panic', 'test', '<p>test</p>'),
(16, 'TEST NIKOLA', '2021-07-10 11:11:28', 'nikola panic', 'TEST NIKOLA', '<p>TEST&nbsp;</p>\r\n<p>NIKOLA</p>'),
(17, 'test', '2021-07-10 12:48:44', 'nikola panic', 'test', '<p>test</p>');

-- --------------------------------------------------------

--
-- Structure de la table `wlms_user`
--

CREATE TABLE `wlms_user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(55) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(320) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `role` varchar(14) NOT NULL DEFAULT 'administrateur',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `wlms_user`
--

INSERT INTO `wlms_user` (`id`, `firstname`, `lastname`, `email`, `pwd`, `role`, `isDeleted`, `status`, `createdAt`, `updatedAt`) VALUES
(22, 'toto', 'toto', 'toto@toto.toto', '$2y$10$cB.txmrB59/xkesUNbMDSOVhGJ75646m2eFEwVZj64QY4ELEFU9gm', 'Administrateur', 1, 0, '2021-03-23 14:29:51', '2021-05-19 21:30:53'),
(23, 'fgddsgf', 'sdgfdg', 'clarence@test.test', '$2y$10$esp6gGifapfM2.2qTo1f1.Lfj8qwDf8cphln2RVR8EqOh6rtF9PvS', 'Administrateur', 1, 0, '2021-03-31 22:21:14', '2021-05-21 10:46:25'),
(24, 'Clarence', 'test', 'clarence@test.test', '$2y$10$i0TuuhX.1blm64GQY/6DZeF6uoUqoFZ3AdRXFKIbwqES1cqEHrR1K', 'Administrateur', 0, 0, '2021-03-31 22:23:18', '2021-06-30 18:36:00'),
(25, 'dfdsf', 'gdfgdfg', 'djfbvjhb@dfhjfd.com', '$2y$10$opnDsC4YPuzEfyzDCt9IbO6OPBt1.dVvlFHXgtg9leml4qUtiKpt.', 'Administrateur', 0, 0, '2021-04-08 09:36:10', '2021-06-30 18:29:28'),
(26, 'nikola', 'panic', 'nikola@panic.fr', '$2y$10$yAqUNn.UCKbKh4UcXGBbt.VVBHbxpP15DhKyvXST9ny/UgI0qy1RC', 'administrateur', 0, 0, '2021-04-28 09:51:39', NULL),
(27, 'hehe', 'test', 'caca@caca.Fr', '$2y$10$fJ00iwFrGsv.W0z0XmSTNOs88VsdpxE8JwP5JLhaZbY7twc9hfjZ6', 'Administrateur', 0, 0, '2021-05-21 10:46:46', '2021-05-21 10:47:17');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `wlms_articles`
--
ALTER TABLE `wlms_articles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `wlms_user`
--
ALTER TABLE `wlms_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `wlms_articles`
--
ALTER TABLE `wlms_articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `wlms_user`
--
ALTER TABLE `wlms_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
