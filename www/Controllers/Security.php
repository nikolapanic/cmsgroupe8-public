<?php
namespace App\Controller;

use App\Core\Security as Secu;
use App\Core\View;
use App\Core\FormValidator;
use App\Core\TokenCreator as Token;
use App\Core\Helpers as H;
use App\Models\User;
use App\Models\Galerie;

session_start();
class Security{
	private $users;
	public function __construct(){
		/**
		 * check if user exist and if token exist to redirect to login
		 */	
		$user = new User();
		$user = $user->checkDataExist();
		$this->users = $user;
		$path = H::getPathname();
		if($path ==='login'||$path ==='s-inscrire'){
			if($this->users < 1 ){
				
			}elseif($path ==='s-inscrire'){
				header('Location: /login');
			}
		}elseif(($_SESSION["token"]==null)||!isset($_SESSION["user_id"])){
			unset($_SESSION["user_id"]);
			unset($_SESSION["token"]);
			header('Location: /login');
			exit();
		}
	}
	
	public function loginAction(){
		if(!isset($_SESSION['token'])){
			$_SESSION['token'] = Token::getInstance();
		}
		
		$user = new User();
		
		$view = new View("login",'connexion');
		if(isset($_SESSION['message'])){
			$view->assign("success",$_SESSION['message']);
			unset($_SESSION['message']);
		}
		
		$formLogin = $user->formLogin($_SESSION["token"]);
		
		if(!empty($_POST)){
			$errors = FormValidator::check($formLogin, $_POST);

			if(empty($errors)){
				$user->setEmail(H::cleanInputData($_POST["email"]));
				$user->setPwd(H::cleanInputData($_POST["pwd"]));
				$answer=$user->getSpecificDbData();
				if($answer==='error token'){
					$view->assign("errors",["err-hacker" => "Mauvais token"]);
				}elseif($answer === "no data"){
					$view->assign("errors",["err-user" => "user doesn't exist"]);
				}elseif(password_verify($_POST["pwd"], $user->getPwd())) {
					$_SESSION["user_id"]=$user->getId();
					$_SESSION["role"]=$user->getRole();
					$_SESSION["profile_picture"]=$user->getPhoto();
					header('Location: /');
					exit();
					
				}else{
					$view->assign("errors",["err-user" => "Wrong password or email"]);
				}
			}else{
				$view->assign("errors", $errors);
			}
			
		}
		//check if the register page is useful
		$view->assign("user_exist",$this->users);
		$view->assign("formLogin", $formLogin);
	}


	public function registerAction(){
		$user = new User();
		$view = new View("register",'connexion');
		if(!isset($_SESSION["token"])){
			$_SESSION["token"] = Token::getInstance();
		}

		if(isset($_SESSION['message'])){
			$view->assign("success",$_SESSION['message']);
			unset($_SESSION['message']);
		}
		
		$form = $user->formRegister($_SESSION["token"]);

		if(!empty($_POST)){

			$errors = FormValidator::check($form, $_POST);

			if(empty($errors)) {
				$user->setFirstname(H::cleanInputData($_POST["firstname"]));
				$user->setLastname(H::cleanInputData(H::clearLastname($_POST["lastname"])));
				$user->setEmail(H::cleanInputData($_POST["email"]));
				
				$user->setRole(H::cleanInputData($_POST["role"]));
				$user->setPwd(password_hash(H::cleanInputData($_POST["pwd"]), PASSWORD_BCRYPT));
				if(!empty($_FILES['image']['name'])){
					//créer l'image dans bdd galerie et ajoute le champs photo dans user
					$img = $_FILES['image'];
					move_uploaded_file($img['tmp_name'],'./public/images/'.$img['name']);
					$galerie = new Galerie();
					$fullname = H::cleanInputData($_POST["firstname"]).' '.H::cleanInputData(H::clearLastname($_POST["lastname"]));
					$galerie->setAuthor($fullname);
					$galerie->setFileName(H::cleanInputData($img['name']));
					$galerie-> setProfilePicture(1);
					$galerie-> save();
					$user->setPhoto(H::cleanInputData($img['name']));
				}else{
					$user->setPhoto('monkey.jpg');
				}
				$answer = $user->save();
				if($answer==='error token'){
					$view->assign("errors",["err-hacker" => "Mauvais token"]);
				}elseif($answer === 'error-user exist'){
					$view->assign("errors",["L'utilisateur existe déjà"]);
				}elseif($answer === 'success') {
					$_SESSION["message"] = "L'utilisateur a bien été créé";
					header( "Location: /login");
				}
			} else{
				$view->assign("errors", $errors);
			}
		}
		$view->assign("form", $form);
	}

	public function create_userAction(){
		$user = new User();
		$view = new View("create_user",'back');
		if(isset($_SESSION['message'])){
			$view->assign("success",$_SESSION['message']);
			unset($_SESSION['message']);
		}
		if(!isset($_SESSION["token"])){
			$_SESSION["token"] = Token::getInstance();
		}

		$form = $user->formRegister($_SESSION["token"]);

		if(!empty($_POST)){
			$errors = FormValidator::check($form, $_POST);
			if(empty($errors)){
				//set les données des champs utilisateur
				$user->setFirstname(H::cleanInputData($_POST["firstname"]));
				$user->setLastname(H::cleanInputData(H::clearLastname($_POST["lastname"])));
				$user->setEmail(H::cleanInputData($_POST["email"]));
				$user->setRole(H::cleanInputData($_POST["role"]));
				$user->setPwd(password_hash(H::cleanInputData($_POST["pwd"]), PASSWORD_BCRYPT));
				//check si une image est importée
				if(!empty($_FILES['image']['name'])){
					//créer l'image dans bdd galerie et ajoute le champs photo dans user
					$img = $_FILES['image'];
					move_uploaded_file($img['tmp_name'],'./public/images/'.$img['name']);
					$galerie = new Galerie();
					$fullname = H::cleanInputData($_POST["firstname"]).' '.H::cleanInputData(H::clearLastname($_POST["lastname"]));
					$galerie->setAuthor($fullname);
					$galerie->setFileName(H::cleanInputData($img['name']));
					$galerie-> setProfilePicture(1);
					$galerie-> save();
					$user->setPhoto(H::cleanInputData($img['name']));
				}else{
					$user->setPhoto('monkey.jpg');
				}
				$answer = $user->save();
				if($answer === 'error-user exist'){
					$view->assign("errors",["err-user" => "user already exist"]);
				}elseif($answer === 'success'){
					$_SESSION['message'] = "L'utilisateur a bien été créé";
					header( "Location: {$_SERVER['REQUEST_URI']}");
				}
			}else{
				$view->assign("errors", $errors);
			}

		}
		
		$view->assign("form", $form);
	}

	public function logoutAction(){
		unset($_SESSION["user_id"]);
		unset($_SESSION["token"]);
		session_destroy();
		header('Location: /login');
		exit();
	}

}