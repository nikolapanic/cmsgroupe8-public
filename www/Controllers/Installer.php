<?php

namespace App\Controller;
use App\Core\View;
use App\Core\ConstantMaker;
// use App\Core\Database;
session_start();

class Installer{
    private $env_data;
    private $pdo;
    
    public function __construct(){
        /**
         * check if isset en files
         */
        if(file_exists(".env")){
            header("Location: /s-inscrire");
            exit();
        }
    }

    public function showInstallerAction(){
        // TODO : retirer espace et charatère spéciaux possible dans le form
        $view = new View("installer",'connexion');
        if (!empty($_POST)) {
            if( count($_POST) == 6){
                foreach ($_POST as $key => $element){
                    if (empty($element)||is_null($element)) {
                        die($key." est vide ou nulle");
                    }
                }
                $_POST['DBPREFIXE']= $_POST['DBPREFIXE'].'_';
                $this->env_data = $_POST;
            }else{
                $view->assign("error", ['error','Veuillez remplir tous les champs']);
            }
        }
       
    }
    public function getEnvData()
    {
        return $this->env_data;
    }
    public function createEnvFiles($data)
    {
        /**
         * creer les fichier env s'il est n'est pas créer
         */
        if (!empty($data)) {
            $envfile = fopen(".env", "w") or die("Unable to open file!");
            $txt = 'ENV=dev';
            fwrite($envfile, $txt);
            fclose($envfile);
            $envfile = fopen(".env.dev", "w") or die("Unable to open file!");
            foreach($data as $key => $value){
                $txt=$key.'='.htmlspecialchars($value)."\n";
                fwrite($envfile, $txt);
            }
            $txt= "DBPORT=3306\nDBDRIVER=mysql";
            fwrite($envfile, $txt);
            fclose($envfile);
            return ['success'=>'Bravo, vos données ont bien été prise en comptes'];
        }else{
            die('no value');
        }
    }

    public function createDb()
    {
        /**
         * creer la base de donnée s'il est n'est pas créer
         */
        try{
            $this->pdo = new \PDO(DBDRIVER.":host=".DBHOST.";port=".DBPORT,DBUSER,DBPWD);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            
            $query = $this->pdo->prepare("CREATE DATABASE IF NOT EXISTS ".DBNAME." CHARACTER SET utf8 COLLATE utf8_general_ci");

            $query->execute();
            
            return ['success'=>'Base de données créée bien créée !'];
        }catch(\exception $e){
            die('erreur '. $e);
        }
    }

}