<?php

namespace App\Controller;

use App\Core\TokenCreator as Token;
use App\Core\View;
use App\Models\User;
use App\Models\Articles;
use App\Models\Pages;
use App\Core\FormValidator;
use App\Models\Theme;
use App\Core\Helpers as H;
use App\Models\Galerie;

session_start();

class Main{
	public function __construct(){
		if((!isset($_SESSION["token"]))||!isset($_SESSION["user_id"])){
			header('Location: /login');
			exit();
		}
	}

	public function defaultAction(){
		$pseudo = "Super Prof"; //Plus tard on le récupèrera depuis la bdd
		$view = new View("home",'back');
		$view->assign("pseudo", $pseudo);
		$view->assign("email", "y.skrzypczyk@gmail.com");		
	}
	public function userAction(){
		$view = new View("user",'back');
		// affiche le message de succes
		if(isset($_SESSION['message'])){
			$view->assign("success",$_SESSION['message']);
			unset($_SESSION['message']);
		}
		// affiche le message d'erreur
		if(isset($_SESSION['erreur'])){
			$view->assign("errors",['error'=>$_SESSION['erreur']]);
			unset($_SESSION['erreur']);
		}

		$user = new User();
		$user->setId($_SESSION["user_id"]);
		// recupère les users
		$answer=$user->getData();
		$view->assign("data",$answer);
		//count user not deleted
		$userNotdelete=0;
		foreach ($answer as $users) {
			if($users['isDeleted']===0){
				$userNotdelete+=1;
			}
		}
		// si un user -> résulat , si + -> résultats
		if ($userNotdelete > 1) {
			$userNotdelete= "$userNotdelete résultats";
			$view->assign('nb_user',$userNotdelete);
		}else{
			$userNotdelete= "$userNotdelete résultat";
			$view->assign('nb_user',$userNotdelete);
		}

		// 2 form -> modif user et delete user
		$form = $user->formModifUser($_SESSION["token"]);
		if(!empty($_GET)){
			if(is_numeric($_GET['delete_id']))
			$form_delete_user = $user->deleteUser($_SESSION["token"],H::cleanInputData($_GET['delete_id']));
		}
		if(!empty($_POST)){
			if($_SESSION["role"]!=='Editeur'){
				if(isset($_POST['isDeleted'])){
					//form delete 
					$errors = FormValidator::check($form_delete_user, $_POST);

					if(empty($errors)) {
						$user->setId($_POST['id']);
						$user-> setIsDeleted($_POST['isDeleted']);
						$answer = $user->save();
						if(is_array($answer)||$answer!=='success'){
							$view->assign("errors",[$answer]);
						}else{
							$_SESSION['message'] = "L'utilisateur a bien été supprimé";
							header( "Location: /user");
						}
					}else{
						$view->assign("errors", $errors);
					}
				}else{
					// form modif
					$errors = FormValidator::check($form, $_POST);

					if(empty($errors)) {
						$user->setId($_POST["id"]);
						$user->setFirstname(H::cleanInputData($_POST["firstname"]));
						$user->setLastname(H::cleanInputData(H::clearLastname($_POST["lastname"])));
						$user->setEmail(H::cleanInputData($_POST["email"]));
						$user->setRole(H::cleanInputData($_POST["role"]));
						$answer = $user->save();
						if($answer!=='success'){
							$view->assign("errors",[$answer]);
						}else{
							$_SESSION['message'] = "L'utilisateur a bien été modifié";
							header( "Location: {$_SERVER['REQUEST_URI']}");
						}
						
					} else{
						$view->assign("errors", $errors);
					}

				}
			}else{
				$_SESSION['erreur']= "Vous n'êtes pas autorisé";
				header( "Location: {$_SERVER['REQUEST_URI']}");
			}
				
		}
		$view->assign("form", $form);
		if(isset($form_delete_user))
		$view->assign("form_delete_user", $form_delete_user);
		
	}
	
	public function commentaireAction()
	{
		$view = new View('comments','back');
		$lorem= 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nemo animi, ad voluptatem consequuntur unde aperiam mollitia, excepturi quibusdam nobis commodi debitis corporis. Eos vero veniam a nulla asperiores quidem corporis.';
		$view->assign("lorem",$lorem);
	}

	public function pagesAction(){

		//recup page
		$view = new View("pages",'back');
		if(isset($_SESSION['message'])){
			$view->assign("success",$_SESSION['message']);
			unset($_SESSION['message']);
		}else if(isset($_SESSION['erreur'])){
			$view->assign("errors",$_SESSION['erreur']);
			unset($_SESSION['erreur']);
		}
		$pages= new Pages;
		$test = $pages->getData();
		$form_delete_page = $pages->formDeletePage($_SESSION["token"]);
		
		if(isset($_GET) && !empty($_GET)){
			if(!empty($_POST)){
				$pages->setId($_GET["delete_id"]);
				$answer = $pages->delete();
				
				if($answer === 'success'){
					$_SESSION['message'] = "la page a bien été supprimée";
					header( "Location: /".Helpers::getPathname());
				} else {
					// $_SESSION['message'] = "le page a bien été supprimée";
					// header( "Location: {$_SERVER['REQUEST_URI']}");
					header( "Location: /".Helpers::getPathname());
					$_SESSION['erreur'] = ['err'=>$answer];
					$view->assign("errors",['err'=>$answer]);
				}
			}
			
		}
		$view->assign("form", $form_delete_page);
		
		$view->assign("page", $test);

	}
	public function create_pageAction(){
		//recup user
		$view = new View("create_page",'back');
		if(isset($_SESSION['message'])){
            $view->assign("success",$_SESSION['message']);
            unset($_SESSION['message']);
        }
		$user = new User();
		$user->setId($_SESSION["user_id"]);
		$answer=$user->getData();
		if($answer === null){
			$view->assign("errors",["err-user" => "user doesn't exist"]);
		}else{
			$view->assign("data",$answer);
		}
		if(isset($_SESSION['message'])){
            $view->assign("success",$_SESSION['message']);
            unset($_SESSION['message']);
        }
		$pages = new Pages;
		$form= $pages->formAddPage($_SESSION["token"]);
		$view->assign("form", $form);

		if(!empty($_POST)){
			$pages->setTitle(htmlspecialchars($_POST["Titre"]));
			$pages->setContent(($_POST["Content"]));
			$pages->setAuthor($_POST["author"]);
			$response = $pages->save();
			header( "Location: {$_SERVER['REQUEST_URI']}", true, 303 );
		}
		$view->assign("form", $form);
	}

	public function settingsAction(){
		$view = new View("settings",'back');
	}
	public function create_articlesAction() {
		$view = new View('create_articles', 'back');
		$user = new User();
		$user->setId($_SESSION["user_id"]);
		$answer=$user->getData();
		if($answer === null){
			$view->assign("errors",["err-user" => "user doesn't exist"]);
		}else{
			$view->assign("data",$answer);
		}
		$articles = new Articles;
		$form = $articles->formAddArticles($_SESSION["token"]);
		if(!empty($_POST)){
			$articles->setTitle(H::cleanInputData($_POST["title"]));
			$articles->setContent(($_POST["Content"]));
			$articles->setAuthor(H::cleanInputData($_POST["author"]));
			$articles->setCategorie(H::cleanInputData($_POST["categorie"]));
			$response = $articles->save();
			$_SESSION['message'] = "L'article a bien été créer";
			header( "Location: /articles");
		}
		$resp=$articles->getData();
		if ($resp == null) {
			$view->assign("errors",["err-categorie" => "Aucune categorie trouvé"]);
		} else {
			$view->assign("cat", $resp);
		}
		$view->assign("form", $form);
	}

	public function articlesAction(){
		//recup artciles
		$view = new View("articles",'back');
		$articles = new Articles;
		if(isset($_SESSION['message'])){
            $view->assign("success",$_SESSION['message']);
            unset($_SESSION['message']);
        }
		$test  = $articles->getData();
		$view->assign("article", $test);
		$nb_articles = count($test);
		$view->assign("nb_articles",$nb_articles);
		//supp
		if (!empty($_GET['delete_id'])) {
			if(is_numeric($_GET['delete_id'])) {
				$form_delete_articles = $articles->deleteArticles($_SESSION["token"],H::cleanInputData($_GET['delete_id']));
				$view->assign("form_delete_articles", $form_delete_articles);
			}
			if (!empty($_POST)) {
				$errors = FormValidator::check($form_delete_articles, $_POST);
			if(empty($errors)) {
				$articles->setId($_POST['id']);
				$answer = $articles->delete();
				if($answer ==='success'){
					//img bien delete de la base de donnée
					$_SESSION['message'] = "Le fichier a bien été supprimé";
					header( "Location: /articles");
				}else{
					$view->assign('errors',[$answer]);
				}
			}
			}
		}
	}

	public function modify_articlesAction() {
		$view = new View("modify_articles",'back');
		
		$articles = new Articles;
		$test  = $articles->getData();
		$uriExploded = explode("/", $_SERVER["REQUEST_URI"]);
		$uri = $uriExploded[2];	
		foreach ($test as $a) {
			if ($uri == $a["id"]) {
				//var_dump($a["id"]);
				$view->assign("test", $a);
			}
		}
		$user = new User();
		$answer=$user->getData();
		
		if(!empty($_POST)){
			$_POST['token'] = $_SESSION["token"];
			$articles->setId(H::cleanInputData($uri));
			$articles->setTitle(H::cleanInputData($_POST["title"]));
			$articles->setContent(($_POST["Content"]));
			$articles->setAuthor(H::cleanInputData($_POST["author"]));
			$articles->setCategorie(H::cleanInputData($_POST["categorie"]));
			$articles->save();
			$_SESSION['message'] = "L'article a bien été modifié";
			header( "Location: /articles");
		}
		if($answer === null){
			$view->assign("errors",["err-user" => "user doesn't exist"]);
		}else{
			$view->assign("data",$answer);
		}
		$resp=$articles->getData();
		if ($resp == null) {
			$view->assign("errors",["err-categorie" => "Aucune categorie trouvé"]);
		} else {
			$view->assign("cat", $resp);
		}
		
	}


	public function galerieAction()
	{
		$view = new View('galerie', 'back');

		if(isset($_SESSION['message'])){
            $view->assign("success",$_SESSION['message']);
            unset($_SESSION['message']);
        }

		$galerie = new Galerie();
		$answer = $galerie->getData();
		$view->assign('data',$answer);
		if(!empty($_GET)){
			if(is_numeric($_GET['delete_id'])){
				$form_delete_img = $galerie->deleteImg($_SESSION["token"],H::cleanInputData($_GET['delete_id']));
				$view->assign('delete_form',$form_delete_img);
			}
			else
				$view->assign('errors',['error'=>'Mauvais ID']);
		}
		if(!empty($_POST)){
			//post delete img
			$errors = FormValidator::check($form_delete_img, $_POST);
			if(empty($errors)) {
				$galerie->setId($_POST['id']);
				$galerie->getSpecificDbData();
				$filename = $galerie->getFileName();
				$answer = $galerie->delete();
				if($answer ==='success'){
					//img bien delete de la base de donnée
					unlink('./public/images/'.$filename);
					$_SESSION['message'] = "Le fichier a bien été supprimé";
					header( "Location: /galerie");
				}else{
					$view->assign('errors',[$answer]);
				}
			}
		}
	}

	public function galerieAddAction()
	{
		$view = new View('galerie_add', 'back');
		$user = new User();
		$user->setId($_SESSION['user_id']);
		$user->getSpecificDbData();
		$lastname = $user->getLastname();
		$firstname = $user->getFirstname();
		if(isset($_SESSION['message'])){
            $view->assign("success",$_SESSION['message']);
            unset($_SESSION['message']);
        }
		$galerie = new Galerie();
		$form = $galerie->addImg($_SESSION["token"]);
		
		if(!empty($_FILES['image']['name'])&&!empty($_POST)){
			$errors = FormValidator::check($form, $_POST);

			if(empty($errors)) {
				$img = $_FILES['image'];
				move_uploaded_file($img['tmp_name'],'./public/images/'.$img['name']);
				$galerie = new Galerie();
				$fullname = "$firstname $lastname";
				$galerie->setAuthor($fullname);
				$galerie->setFileName(H::cleanInputData($img['name']));
				$galerie-> setProfilePicture(0);
				$galerie-> save();
				$_SESSION['message'] = "Le fichier a bien été ajouté";
				header( "Location: /galerie");
			}
		}
		$view->assign("form", $form);
	}

	public function themeAction() {
		$view = new View("theme",'back');
		$theme = new Theme();
		$test  = $theme->getData();
		$view->assign("theme",$test);
		$nb_themes = count($test);
		$view->assign("nb_articles",$nb_themes);
	}
}