<?php

namespace App\Core;

class Helpers
{

	public static function clearLastname($lastname){
		return mb_strtoupper(($lastname));
	}
	public static function getPathname(){
		$uriExploded = explode("?", $_SERVER["REQUEST_URI"]);
		$pathname = trim($uriExploded[0], '/');
		return $pathname;
	}
	public static function cleanInputData($data)
	{
		return htmlspecialchars(trim($data));
	}
	public static function checkPhotoExist($photo)
	{
		if (file_exists('./public/images/'.$photo)) {
			return $photo;
		}else{
			return 'monkey.jpg';
		}
	}
	public static function truncateFilename($string){
		$string = explode('.',$string);
		if(strlen($string[0])>= 12){
			return substr($string[0], 0,12).'...';
		}else{
			return $string[0];
		}
	}
	public static function changeToken(){
		$_SESSION['token'] = TokenCreator::getInstance();
	}
}