<?php

namespace App\Core;
use Exception;
use App\Models\User;
use App\Models\Articles;
use App\Models\Pages;
use App\Models\Theme;
class Database
{
	// TODO : faire le jointure de table (SELECT * FROM A INNER JOIN B ON A.key = B.key)
	private $pdo;
	private $table;

	public function __construct(){
		try{
			$this->pdo = new \PDO(DBDRIVER.":dbname=".DBNAME.";host=".DBHOST.";port=".DBPORT,DBUSER,DBPWD);

			if(ENV == "dev"){
				$this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
	    		$this->pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
    		}

		}catch(\Exception $e){
			die("Erreur SQL " . $e->getMessage());
		}

		$getCalledClassExploded = explode("\\", get_called_class()); //App\Models\User
		$this->table = DBPREFIXE.strtolower(end($getCalledClassExploded));
		$check_table_answer= $this->checkTableExist($this->table);
		if(!$check_table_answer){
			try {
				get_called_class()::boot($this->pdo,$this->table);
			} catch (\Throwable $th) {
				die($th);
			}
			
		}else{
			
		}
	}

	public function checkTableExist($table){
		try {
			$result = $this->pdo->query("SELECT 1 FROM $table LIMIT 1");
		} catch (Exception $e) {
			// We got an exception == table not found
			return FALSE;
		}
	
		// Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
		return $result !== FALSE;
		
	}


	public function checkDataExist(){
		$query = $this->pdo->prepare("SELECT COUNT(*) AS num FROM " . $this->table);
		$query->execute();
		$data = $query->fetch(\PDO::FETCH_ASSOC);
		return $data["num"];
	}

	public function getSpecificDbData(){
		
		if(is_null($this->getId())){
			//login 
			$query = $this->pdo->prepare("SELECT * FROM ".$this->table." WHERE email=:email");
			$query->execute(array(':email'=>$this->getEmail()));
		}else{
			$query = $this->pdo->prepare("SELECT * FROM ".$this->table." WHERE id=:id");
			$query->execute(array(':id'=>$this->getId()));
		}
		$data = $query->fetch(\PDO::FETCH_ASSOC);
		if(gettype($data)==='boolean'){
			return "no data";
		}else{
			$this->setId($data["id"]);
			$table = explode("\\", get_called_class());
			$table = strtolower(end($table));
			switch ($table) {
				case 'user':
					$this->setEmail($data['email']);
					$this->setPwd($data['pwd']);
					$this->setRole($data['role']);
					$this->setPhoto($data['photo']);
					$this->setFirstname($data['firstname']);
					$this->setLastname($data['lastname']);
					break;

				case 'galerie':
					$this->setFileName($data['filename']);
					$this->setAuthor($data['author']);
					break;
				
				default:
					# code...
					break;
			}
			//etc.. a rajouter
		}
		
	}

	public function getData(){
		$query = $this->pdo->prepare("SELECT * FROM ".$this->table);
		$query->execute();
		$data = $query->fetchAll();
	
		if(empty($data)){
			return null;
		}
		return $data;
	}

	//============ Begin Delete function ==============//

	public function delete(){
		if($_POST["token"]===$_SESSION["token"]){
			// $id == null -> erreur
			if(is_null($this->getId())){
				//INSERT
				//check que l'user n'existe pas
				return "pas d'id";
			}else{
				//Delete
				$query = $this->pdo->prepare("DELETE FROM ".$this->table." WHERE id = :id");
				$query->bindParam(':id', $this->getId());
				$query->execute();
				
				return 'success';	
			}
		
		} else {
			return "erreur token";
		}

	}

//=========== End Delete function ==============//

	public function save(){
		if($_POST["token"]===$_SESSION["token"]){
			
			$columns = array_diff_key (
				get_object_vars($this),
				get_class_vars(get_class())
			);
			//INSERT OU UPDATE
			// $id == null -> INSERT SINON UPDATE
			if(is_null($this->getId())){
				//INSERT
				//check que l'user n'existe pas
				if($this->table === (DBPREFIXE.'user')){
					$checkUserNotExist = $this->pdo->prepare("SELECT id FROM ".$this->table." WHERE email = :email");
					$checkUserNotExist->bindParam(':email', $columns["email"]);
					$checkUserNotExist->execute();
					$checkUserNotExist = $checkUserNotExist->fetch(\PDO::FETCH_ASSOC);
					if(gettype($checkUserNotExist) !== 'boolean'){
						//return {"id"=>6}
						return "error-user exist";
					}
				}
				//return false if no id
				$query = $this->pdo->prepare("INSERT INTO ".$this->table." (".
					implode(",", array_keys($columns))
				.") 
				VALUES ( :".
					implode(",:", array_keys($columns))
				." );");
				$query->execute($columns);
				return 'success';
			}else{
				//UPDATE
				if(!empty($this->table === (DBPREFIXE.'user') && $this->getEmail())){
					//update user data
					// verifie quand on modif un mail, on a paas le meme qu'un autre
					$checkUserNotExist = $this->pdo->prepare("SELECT email FROM ".$this->table." WHERE email = :email");
					$checkUserNotExist->bindParam(':email', $columns["email"]);
					$checkUserNotExist->execute();
					$checkUserNotExist->fetch(\PDO::FETCH_ASSOC);
					if( $checkUserNotExist->rowCount() > 1 ) { # If rows are found for query
						return ["error"=>"Utilisateur déjà existant avec le même email"];
					}
				}
				$data =[];
				$update = "UPDATE ".$this->table." SET "." ";
				foreach ($columns as $key => $value) {
					if($value !== null){
						$update .= "$key = :$key, "; 
						$data[$key]=$value;
					}
				} 
				$update = mb_substr($update, 0, -2);
				$update.= " WHERE id =:id";
				$data["id"]=$this->getId();
				$query = $this->pdo->prepare($update);
				$query->execute($data);
				
				return 'success';	
			}
		}else{
			return 'error token';
		}
		/*
		//Prepare
		$query = $this->pdo->prepare("INSERT INTO wpml_user (firstname, lastname, email) 
				VALUES ( :firstname , :lastname , :email );");  // 1

		//Executer
		$query->execute(
				[
					"firstname"=>"Yves",
					"lastname"=>"Skrzypczyk",
					"email"=>"y.skrzypczyk@gmail.com"
				]
		);
		*/

	}

}
