<?php

namespace App\Core;

class FormBuilder
{

	public static function render($form,$data = null){
		$html = "<form 
				method='".($form["config"]["method"]??"GET")."' 
				".(!empty($form["config"]["id"])?"id=".$form["config"]["id"]:"")."
				".(isset($form["config"]["enctype"])?"enctype=multipart/form-data":"")."
				class='".($form["config"]["class"]??"")."' 
				action='".($form["config"]["action"]??"")."'>";
			foreach ($form["inputs"] as $name => $configInput) {
				if(is_null($data)){
					if($name !== 'token'&& $configInput["type"]!=="radio" && $configInput["type"]!=="checkbox"){
						$html .="<label for='".($configInput["id"]??"")."'>".($configInput["label"]??"")." </label>";
					}
				}
				if($configInput["type"] == "select"){
					$html .= self::renderSelect($name, $configInput, $data);
				}elseif($configInput["type"]=="radio"||$configInput["type"]=="checkbox"){
					$html .= self::renderRadio_Checkbox($name,$configInput,$data);
				}else if($name == 'Content') {
					$html .= self::renderTextArea($name, $configInput);
				}else{
					$html .= self::renderInput($name, $configInput, $data);
				}

			}
		

		if(isset($form["config"]["id"])&&$form["config"]["class"]=='delete'){
			//bouton delete user
			$html .= "<button type='submit'>".($form["config"]["submit"]??"Valider")."</button>";
		} else if (isset($form["config"]["id"])&&$form["config"]["id"]=='form_articles') {
			//
		}
		
		else{
			$html .= "<div id=\"formButton\"><input type='submit' class='button' value=\"".($form["config"]["submit"]??"Valider")."\"></div>";
		}
		

		$html .= "</form>";



		echo $html;
		
	}


	public static function renderInput($name, $configInput,$data=null){
		if(is_null($data)){
			return "<input 
						name='".$name."' 
						type='".($configInput["type"]??"text")."'
						id='".($configInput["id"]??"")."'
						class='".($configInput["class"]??"")."'
						placeholder='".($configInput["placeholder"]??"")."'
						".(!empty($configInput["required"])?"required='required'":"")."
						".(!empty($configInput["accept"])?"accept=".$configInput['accept']:"")."
						".(!empty($configInput["value"])?"value='".$configInput["value"]."'":"")."
					>".($name==='pwd'?"<span id=\"show-password\" class='iconify' data-inline='false' data-icon='el:eye-open'></span>
					":"")."<br>";
		}else{
			// data passé dans le form pour modifier les donnée
			foreach($data as $key => $value){
				
				if($key===$name){
					$data_value = $value;
				}
			}
			return "<input 
						name='".$name."' 
						type='".($configInput["type"]??"text")."'
						".(!empty($configInput["id"])?"id=".$configInput["id"]:"")."
						class='".($configInput["class"]??"")."'
						".(!empty($configInput["required"])?"required='required'":"")."
						".(!empty($data_value)?"value=".$data_value:"value=".$configInput["value"])."
					>".($name==='pwd'?"<span id=\"show-password\" class='iconify' data-inline='false' data-icon='el:eye-open'></span>
					":"")."<br>";
		}
		
	}




	public static function renderSelect($name, $configInput, $data){
		if (is_null($data)) {
			$html = "<select name='".$name."' id='".($configInput["id"]??"")."'
			class='".($configInput["class"]??"")."'>";


		foreach ($configInput["options"] as $key => $value) {
			$html .= "<option value='".$key."'>".$value."</option>";
		}

		$html .= "</select><br>";

		return $html;
		} else {
			foreach($data as $key => $value){
				var_dump($data);
			}
		}
		
	}
	public static function renderRadio_Checkbox($name, $configInput,$data){
		$html='';
		foreach($configInput["label"] as $key => $value){
			if($value==='checked'){
				$html .= "<input type='".$configInput["type"]."' 
				name='".$name."' 
				id='".$key."' 
				value='".$key."' checked >";
				
			$html .= "<label for='".$key."'>".$key."</label><br>";
			}else{
				$html .= "<input type='".$configInput["type"]."' 
				name='".$name."' 
				id='".$value."' 
				value='".$value."'
				".($value===$data['role']?'checked': '')."
				>";
				
				$html .= "<label for='".$value."'>".$value."</label><br>";
			}
			
		}
		return $html;
	}

	public static function renderTextArea($name, $configInput) {
		return "<textarea
		name='".$name."' 
		".(!empty($configInput["id"])?"id=".$configInput["id"]:"")."
		class='".($configInput["class"]??"")."'
		".(!empty($configInput["required"])?"required='required'":"")."
		></textarea>";
	}
}