<?php

namespace App\Core;

class FormValidator
{
	public static $allow_ext = ['/jpg','/png','/svg','jpeg'];

	/**
	 * Check la data des inputs pour eviter les tentative de hack
	 *
	 * @param [type] $form 
	 * $data = $_POST
	 * @return void ou tableau d'erreur
	 */
	
	public static function check($form, $data){
		$errors = [];
		$Files_exist = false;
		$count_data = count($data);
		/* 
			$_FILES n'est pas pris en compte dans les inputs $_POST
			donc on l'ajoute au count data s'il n'est pas vide
		*/
		if(!empty($_FILES)){
			$Files_exist = true;
			$count_data += count($_FILES);
		}
		
		if( $count_data == count($form["inputs"])){
			if($Files_exist){
				/* 
					code qui check les erreur de l'envoi d'images
					(bonne extension, pas d'erreur dans $_FILES) 
				*/
				foreach($_FILES as $inputs){
					foreach($inputs as $key => $element){
						if($key=='error' && !empty($element)&&$element!=4){
							$errors[] = 'FILES error : '.$element;
						}
					}
					if($inputs['type']){
						$ext = strtolower(substr($inputs['type'],-4));
						if(!in_array($ext,FormValidator::$allow_ext)){
							$errors[] = "Mauvais format d'image (SVG, PNG et JPEG uniquement)";
						}
					}
				}
			}
			if(isset($data['pwdConfirm'])){
				if($data['pwdConfirm']!==$data['pwd']){
					$errors[] = "Le mot de passe diffère du mot de passe confirmé";
				}
			}
			foreach ($form["inputs"] as $name => $configInput) {
				if(!empty($configInput["minLength"]) &&
					is_numeric($configInput["minLength"]) &&
					strlen($data[$name]) < $configInput["minLength"]
					){
					$errors[] = $configInput["error"];
				}
			}
		}else{
			$errors[] = "Tentative de Hack";
		}

		return $errors;
	}


}