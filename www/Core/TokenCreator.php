<?php

namespace App\Core;

class TokenCreator{
 
    private static $_instance = null;

    private $token = null;
   /**
    * Constructeur de la classe
    *
    * @param void
    * @return void
    */
    private function __construct() {  

        $this->token = hash('sha256',uniqid());
      
        return $this->token;
    }
   
   /**
    * Méthode qui crée l'unique instance de la classe
    * si elle n'existe pas encore puis la retourne.
    *
    * @param void
    * @return Singleton
    */
    public static function getInstance() {
        if(is_null(self::$_instance)) {
            self::$_instance = new TokenCreator(); 
        }

        return self::$_instance->token;
    }
}