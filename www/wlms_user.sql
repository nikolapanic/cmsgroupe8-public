-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : database
-- Généré le : ven. 21 mai 2021 à 14:45
-- Version du serveur :  5.7.32
-- Version de PHP : 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `mvcdocker2`
--

-- --------------------------------------------------------

--
-- Structure de la table `wlms_user`
--

CREATE TABLE `wlms_user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(55) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(320) NOT NULL,
  `pwd` varchar(250) NOT NULL,
  `role` varchar(14) NOT NULL DEFAULT 'administrateur',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `wlms_user`
--

INSERT INTO `wlms_user` (`id`, `firstname`, `lastname`, `email`, `pwd`, `role`, `status`, `isDeleted`, `createdAt`, `updatedAt`) VALUES
(1, 'clarence', 'clarence', 'potel.clarence@gmail.com', '$2y$10$HkYA1XEV.EzjkGiNqJsxiO5gKzm0EPSW/whLQdnnisIYrChlqLeYS', 'Administrateur', 0, 0, '2021-05-21 14:41:24', NULL),
(2, 'hjdkshvl', 'vndjsb', 'jdkvhd@vfjdkl.vifdkl', '$2y$10$WjY42TnhTWxILtWhSAMsIeF17rWRpuQka2VN6D81Up0FY9zGs6wQe', 'Administrateur', 0, 0, '2021-05-21 14:41:55', NULL),
(3, 'dfvjvhdl', 'vfjghvkj', 'hdjkvd@fjkdlsv.sj', '$2y$10$fSXAOSJM8qARVcf3hHFD0eQayaL65nl2nwGEmmprbhRlbzT8WDcq2', 'Editeur', 0, 0, '2021-05-21 14:42:12', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `wlms_user`
--
ALTER TABLE `wlms_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `wlms_user`
--
ALTER TABLE `wlms_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
