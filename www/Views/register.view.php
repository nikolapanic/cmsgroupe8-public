<section class="container-fluid">
    <div class="row row-connexion">
        <div class="col-5 content register">
            <div class="col-inner">
                <h1 id="title">Power rent</h1>
                <h2 id="description">Power Rent la référence pour vos site de <br/> gestion et location de véhicules en un clic </h2>

				<h4>Création du compte admin &#128195;</h4>
				<?php if(isset($errors)):?>
				<div class="card-message card-message-error">
					<p>
						<?php foreach ($errors as $error):?>
							<?=$error;?><br/>
						<?php endforeach;?>
					</p>
					<button id="close-message-button">X</button>
				</div>
				<?php endif;?>
				<?php if(isset($success)):?>
				<div class="card-message card-message-success">
					<p><?=$success?> </p>
					<button id="close-message-button">X</button>
				</div>
				<?php endif;?>
				
				<?php App\Core\FormBuilder::render($form)?>
			</div>
        </div>
    </div>
</section>


