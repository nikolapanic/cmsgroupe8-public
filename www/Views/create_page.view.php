
<div onclick="close_preview()" class="bg-preview" id="bg_preview"></div>
    <div class="preview-articles" id="preview_articles">
        <span onclick="close_preview()" class="close_button">X</span>
        <div id="title_preview"></div>
        <div id="preview_content"></div>
    </div>
    
<form method="POST">
<h1 id="title">Créer une page</h1>
<section id="create_articles">
<div class="left-block">
    <?php App\Core\FormBuilder::render($form)?>
</div>

    <div class="right-block">
    <span class="preview">
            <a id=btn-preview onclick="preview()"><span class="iconify" data-inline="false" data-icon="bi:eye-fill" style="color: #000; margin-left:5px; margin-right:10px; width:18px;"></span>Preview</a>
        </span>
        <div class="author">
            <p>Auteur</p>
            <select name="author" id="author-select">
                <?php foreach ($data as $users):?>
                    
                    <option><?= $users["firstname"];?> <?= $users["lastname"]; ?></option>
                <?php endforeach;?>
            </select>
        </div>
    
        <div class="status">
            <p>Etat</p>
            <div class="states">
                <p>Modifié le : </p>
                <p>Publié le : </p>
            </div>
        </div>
        <div class="btn-publish-delete">
            <span class="submit-btn">
                <button type="submit" id="btn-submit">Publier</button>
            </span>
            <span class="delete-btn">
                <button id="btn-delete">Supprimer</button>
            </span>
        </div>
    </div>
    <?php if(isset($errors)):?>

<div class="card-message card-message-error">
<p>
    <?php foreach ($errors as $error):?>
        <?=$error;?><br/>
    <?php endforeach;?>
</p>
<button id="close-message-button">X</button>
</div>
<?php endif;?>
    </form>
    
</section>

