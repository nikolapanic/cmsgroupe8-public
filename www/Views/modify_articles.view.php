 

<div onclick="close_preview()" class="bg-preview" id="bg_preview"></div>
    <div class="preview-articles" id="preview_articles">
        <span onclick="close_preview()" class="close_button">X</span>
        <div id="title_preview"></div>
        <div id="preview_content"></div>
    </div>
    
<form method="POST" action="">
<input type="hidden" name="token">
<h1 id="title">Modification de l'article : <?= $test["title"];?></h1>
<section id="create_articles">
<div class="left-block">
    <input id="name_article" type="text" name="title" value="<?= $test["title"];?>" style="margin-top:20px;" maxlength="80">
    <div class="content">
            <label for="mytextarea" style="margin-bottom:10px;">Contenu</label>
            <textarea  id="mytextarea" name="Content" ></textarea>
        </div>
</div>
    </div> 
    <div class="right-block">
    <span class="preview">
            <a id="btn-preview"‚ onclick="preview()"><span class="iconify" data-inline="false" data-icon="bi:eye-fill" style="color: #000; margin-left:5px; margin-right:10px; width:18px;"></span>Preview</a>
        </span>
        <div class="author">
            <p>Auteur</p>
            <select name="author" id="author-select">
            <option><?= $test["author"];?></option>
                <?php foreach ($data as $users):?>
                    <?php if($users["firstname"].' '. $users["lastname"] == $test["author"]) :?>
                    <?php else:?>
                        <option><?= $users["firstname"];?> <?= $users["lastname"]; ?></option>
                    <?php endif;?>
                    
                <?php endforeach;?>
            </select>
        </div>
       
        <div name="categorie" class="categorie">
            <label for="categorie">Catégorie :</label>
            <input list="categorie" name="categorie" id="list_categorie" placeholder="<?= $test["categorie"];?>" required>
            <datalist id="categorie" >
                <?php foreach ($cat as $categorie):?>
                    
                    <?php if($categorie["categorie"] == $test["categorie"]) :?>
                    <?php else:?>
                        <option value="<?= $categorie["categorie"];?>"></option>
                    <?php endif;?>
                    
                <?php endforeach;?>
            </datalist>
        </div>
        <div class="status">
            <p>Etat</p>
            <div class="states">
                <p>Modifié le : <?php $new_time = date("Y-m-d H:i:s", strtotime('+2 hours')); echo $new_time; ?></p>
                <p>Publié le : <?= $test["date"];?></p>
            </div>
        </div>
        <div class="btn-publish-delete">
            <span class="submit-btn">
                <button type="submit" id="btn-submit">Publier</button>
            </span>
            <a href="/articles" id="btn-delete">Annuler</a>
        </div>
    </div>
    </form>
    
</section>
<script>
            window.addEventListener('load', (event) => {
                tinyMCE.get('mytextarea').setContent(`<?= $test["Content"];?>`);});
        </script>

