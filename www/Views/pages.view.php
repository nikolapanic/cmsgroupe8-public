    <div class="row">
        <div class="col-11">
            <h1>Pages</h1>
        </div>
    </div>
    
   <!-- message d'erreur -->
    <?php if(isset($errors)):?>
    <div class="card-message card-message-error">
        <p>
            <?php foreach ($errors as $error):?>
                <?=$error;?><br/>
            <?php endforeach;?>
        </p>
        <button id="close-message-button">X</button>
    </div>
    <?php endif;?>
    <!-- message de success -->
    <?php if(isset($success)):?>
    <div class="card-message card-message-success">
        <p><?=$success?></p>
        <button id="close-message-button">X</button>
    </div>
    <?php endif;?>

    <!-- delete page  -->
    <?php if(isset($_GET) && !empty($_GET)) : ?>
    <div class="delete-user ">
        <div class="card-message card-message-error">
            <p>Êtes-vous sûr de vouloir supprimer cette page ?</p>
            <div class="bouton">
            
                <?php App\Core\FormBuilder::render($form); ?>
           
                <button>
                    <a href="/pages">Non</a>
                </button>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <!-- End delete page -->

    <div class="row head-container">
        <div class="col-3 results">
            <h3>2 résultats</h3>
        </div>
        <div class="col-8 head-container-btn">
        <a href="/create_page">
            <button class="button-secondary">
                <span class="iconify plus" data-icon="akar-icons:plus" data-inline="false"></span>
                <p> Ajouter une page</p>
            </button></a>
            <div class="custom-select">
                <select name="filter" id="">
                    <option value="default" selected>Filtre</option>
                    <option value="nom">nom</option>
                    <option value="date">date</option>
                </select>
                <span class="iconify" data-icon="bi:filter" data-inline="false"></span>
            </div>
        </div>
    </div>
    <div class="row row-card">
    <?php if(isset($page) && !empty($page)) : ?>
        <?php foreach($page as $pages) : ?>
        <div class="col-5 col-lg-5">
            <div class="col-inner card">
                <div class="flex">
                    <!-- <img class="user-img" src="../public/asset/todoroki.jpg" alt="user"> -->
                    <div class="info">

                    <h2><?= $pages["title"];?></h2>
                    <p>Auteur: <?= $pages["author"];?></p>
                    <p>Date de création: <?= $pages["date"];?></p>
                    </div>
                </div>
                <div>
                    <div class="icon-btn icon-btn-articles">
                        <?php if(isset($_GET) && !empty($_GET)):  ?>
                        <button>
                            <span class="iconify modifier" data-inline="false" data-icon="ant-design:edit-outlined"></span>
                        </button>
                        <button>
                            <span class="iconify delete" data-inline="false" data-icon="carbon:delete"></span>
                        </button>

                        <?php else: ?>
                            <button>
                            <span class="iconify modifier" data-inline="false" data-icon="ant-design:edit-outlined"></span>
                        </button>
                        <button>
                            <a href="<?=$_SERVER['REQUEST_URI']?>?delete_id=<?= $pages["id"];?>">
                                <span class="iconify delete" data-inline="false" data-icon="carbon:delete"></span>
                            </a>
                        </button>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        
        </div>

        <?php endforeach ?>
        <?php else: ?>    
        <div class="col-5 col-lg-5">
            <p>Pas de page</p>
        </div>
        <?php endif; ?>
    </div>
    <div class="row add-btn">
        <div class="col-11">
            <div class="line"></div>
        </div>
        <a href="/create_page">
        <button>
            <span class="iconify icon-plus" data-icon="bi:plus-circle-fill" data-inline="false"></span>
        </button></a>
    </div>
