<section class="container-fluid">
    <div class="row row-connexion">
        <div class="col-5 content">
            <div class="col-inner test">
                <h1 id="title">Installation de Power Rent</h1>
                <h2 id="description">Power Rent la référence pour vos site de <br/> gestion et location de véhicules en un clic </h2>

                <?php if(isset($errors)):?>
                    <div class="card-message card-message-error">
                        <p>
                            <?php foreach ($errors as $error):?>
                                <?=$error;?><br/>
                            <?php endforeach;?>
                        </p>
                        <button id="close-message-button">X</button>
                    </div>
                <?php endif;?>
                <div class="wrapper">
                    <form class="form_builder" action="" method="post">
                        <label>Nom du site</label>
                        <input type="text" placeholder="Exemple: jevendsmavoiture" name="SITE_NAME" require>
                        <label>Nom de la base de donnée</label>
                        <input type="text" value="mvcdockertest" name="DBNAME" require>
                        <label>Identifiant</label>
                        <input type="text" value="root" name="DBUSER" require>
                        <label>Mot de passe</label>
                        <input type="password" require name="DBPWD">
                        <label>Addresse de la base de donnée</label>
                        <input type="text" value="database" require name="DBHOST">
                        <label>Préfixe des tables</label>
                        <input type="text" value="wlms" name="DBPREFIXE" require>
                        <button type="submit" class="button">Validation</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
