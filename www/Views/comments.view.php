<div class="row">
    <div class="col-11">
        <h1>Commentaires</h1>
    </div>
</div>
<div class="row head-container">
    <div class="col-3 results">
        <h3>1 résultat</h3>
    </div>
    <div class="col-8 head-container-btn">
        <div class="custom-select">
            <select name="filter" id="">
                <option value="default" selected>Filtre</option>
                <option value="nom">nom</option>
                <option value="date">date</option>
            </select>
            <span class="iconify" data-icon="bi:filter" data-inline="false"></span>
        </div>
    </div>
</div>

<?php for ($i=0; $i < 2; $i++) : ?>
<div class="row row-card comment-page">
    <div class="col-11">
        <div class="col-inner card">
            <div class="flex">
                <div class="info">
                    <h2>Potel Clarence</h2>
                    <p>page</p>
                    <p>date</p>
                </div>

                <?php if(strlen($lorem)>=150) : ?>
                    <div class="commentaire">
                        <p class="full-comment-<?= $i ?> display-none"><?=$lorem ?></p>
                        <p class="show-read-more-<?= $i ?>"><?= substr($lorem, 0,150) ?><span class="etc">...</span></p>
                        <a href="javascript:void(0);" class="read-more read-more-<?= $i ?>" onclick="readMore(<?= $i ?>)">Voir plus...</a>
                    </div>
                    <div class="icon-btn">
                        <button>
                            <span class="iconify delete" data-inline="false" data-icon="carbon:delete"></span>
                        </button>
                    </div>
                <?php else: ?>
                    <div class="commentaire">
                        <p class="full-comment"><?= $lorem ?></p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php endfor ?>