<div class="row">
    <div class="col-11">
        <h1>Utilisateurs</h1>
    </div>
</div>
<!-- message d'erreur -->
<?php if(isset($errors)):?>
<div class="card-message card-message-error">
    <p>
        <?php foreach ($errors as $error):?>
            <?=$error;?><br/>
        <?php endforeach;?>
    </p>
    <button id="close-message-button">X</button>
</div>
<?php endif;?>
<!-- message de success -->

<?php if(isset($success)):?>
<div class="card-message card-message-success">
    <p><?=$success?></p>
    <button id="close-message-button">X</button>
</div>
<?php endif;?>
	
<!-- delete user  -->
<?php if(isset($_GET['delete_id'])&&is_numeric($_GET['delete_id'])) : ?>
<div class="delete-user">
    <div class="card-message card-message-error">
        <p>Êtes-vous sûr de vouloir supprimer cet utilisateur ?</p>
        <div class="bouton">
            <?php  App\Core\FormBuilder::render($form_delete_user) ?>
            <a href="/user"><button>Non</button></a>
        </div>
    </div>
</div>

<?php endif; ?>


<div class="row head-container">
    <div class="col-3 results">
        <h3><?= $nb_user ?></h3>
    </div>
    <div class="col-8 head-container-btn">
        <a href="/user/create_user">
            <button class="button-secondary">
                <span class="iconify plus" data-icon="akar-icons:plus" data-inline="false"></span>
                <p> Ajouter un utilisateur</p>
            </button>
        </a>
        <div class="custom-select">
            <select name="filter" id="">
                <option value="default" selected>Filtre</option>
                <option value="nom">nom</option>
                <option value="date">date</option>
            </select>
            <span class="iconify" data-icon="bi:filter" data-inline="false"></span>
        </div>
    </div>
</div>
<div class="row row-card">
    <?php foreach ($data as $users):?>
    <?php if($users['isDeleted']===0): ?>
    <div class="col-5 col-lg-5">
        <div class="col-inner card">
            <div class="flex">
                <img class="user-img" src="../public/images/<?= App\Core\Helpers::checkPhotoExist($users['photo'])?>" alt="user">
                <div id="user<?= $users["id"]?>" class="info">
                    <h2><?= $users["firstname"];?> <?= $users["lastname"]; ?></h2>
                    <h3><?= ucfirst($users["role"]);?></h3>
                    <p><?= $users["email"];?></p>
                    
                </div>
                
                <div id="user_modify<?= $users["id"]?>" class="info modif-user display-none">
                    
                    <?php App\Core\FormBuilder::render($form,$users)?>
                </div>
            </div>
            <div class="icon-btn">
                <button class="default-btn <?= $users['id'] ?>" onclick="userModify(<?= $users['id'] ?>)">
                    <span class="iconify modifier" data-inline="false" data-icon="ant-design:edit-outlined"></span>
                </button>

                <button class="default-btn <?= $users['id'] ?>">
                    <a href="<?= $_SERVER['REQUEST_URI']?>?delete_id=<?= $users['id'] ?>">
                    <span class="iconify delete" data-inline="false" data-icon="carbon:delete"></span>
                    </a>
                </button>

                <button class="close-btn<?= $users['id'] ?> display-none" onclick="userModify(<?= $users['id'] ?>)">
                    <span class="iconify delete" data-inline="false" data-icon="jam:close-circle"></span>
                </button>
            </div>
        </div>
    </div>
    <?php endif;?>
    <?php endforeach;?>
</div>
<!-- <div class="delete-user display-none">
    <div class="card-message card-message-error">
        <p>Êtes-vous sûr de vouloir supprimer cet utilisateur ?</p>
        <div class="bouton">
            <button>Oui</button>
            <button onclick="userDelete()">Non</button>
        </div>
    </div>
</div> -->
<div class="row add-btn">
    <div class="col-11">
        <div class="line"></div>
    </div>
    <a href="/user/create_user">
    <button>
        <span class="iconify icon-plus" data-icon="bi:plus-circle-fill" data-inline="false"></span>
    </button>
    </a>
</div>