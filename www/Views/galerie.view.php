<div class="row">
    <div class="col-11">
        <h1>Galerie</h1>
    </div>
</div>
<div class="row head-container">
    <div class="col-3 results">
        <?php if (!empty($data) && $data) : ?>
            <h3><?= count($data) ?><?= count($data) > 1 ? ' résultats' : ' résultat' ?></h3>
        <?php else : ?>
            <h3>0 résultat</h3>
        <?php endif ?>
    </div>
    <div class="col-8 head-container-btn">
        <a href="/galerie/add">
            <button class="button-secondary">
                <span class="iconify plus" data-icon="akar-icons:plus" data-inline="false"></span>
                <p> Ajouter une image</p>
            </button>
        </a>
        <div class="custom-select">
            <select name="filter" id="">
                <option value="default" selected>Filtre</option>
                <option value="nom">nom</option>
                <option value="date">date</option>
            </select>
            <span class="iconify" data-icon="bi:filter" data-inline="false"></span>
        </div>
    </div>
</div>

<!-- message de success -->
<?php if (isset($success)) : ?>
    <div class="card-message card-message-success">
        <p><?= $success ?></p>
        <button id="close-message-button">X</button>
    </div>
<?php endif; ?>

<!-- delete img  -->
<?php if (isset($_GET['delete_id']) && is_numeric($_GET['delete_id'])) : ?>
    <div class="delete-user">
        <div class="card-message card-message-error">
            <p>Êtes-vous sûr de vouloir supprimer ce fichier ?</p>
            <div class="bouton">
                <?php App\Core\FormBuilder::render($delete_form) ?>
                <a href="/galerie"><button>Non</button></a>
            </div>
        </div>
    </div>

<?php endif; ?>

<div class="row row-card galerie">
    <?php if (!empty($data) && $data) : ?>
        <?php foreach ($data as $element) : ?>
            <div class="col-3 col-md-5">
                <div class="col-inner card">
                    <div class="flex header-card">
                        <h2><?= App\Core\Helpers::truncateFilename($element['filename'])  ?></h2>
                        <div class="icon-btn">
                            <button>
                                <span class="iconify modifier" data-inline="false" data-icon="ant-design:edit-outlined"></span>
                            </button>
                            <button>
                                <a href="<?= $_SERVER['REQUEST_URI'] ?>?delete_id=<?= $element['id'] ?>">
                                    <span class="iconify delete" data-inline="false" data-icon="carbon:delete"></span>
                                </a>
                            </button>
                        </div>
                    </div>
                    <div class="image-preview">
                        <img src="../public/images/<?= $element['filename'] ?>" alt="">
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else : ?>
        <div class="col-11">
            <p>Pas d'image</p>
        </div>
    <?php endif ?>
</div>

<div class="row add-btn">
    <div class="col-11">
        <div class="line"></div>
    </div>
    <a href="/galerie/add">
        <button>
            <span class="iconify icon-plus" data-icon="bi:plus-circle-fill" data-inline="false"></span>
        </button>
    </a>
</div>