<!DOCTYPE html>
<html lang="FR">
<head>
	<meta charset="UTF-8">
	<title>Template de FRONT</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="../../public/css/main.css">
	<!-- <script src=""></script> -->
	<!-- <meta name="description" content="ceci est la description de ma page"> -->
</head>
<body>
	<header>
		<h1>Template front</h1>
	</header>

	<!-- intégration de la vue -->
	<?php include $this->view ;?>

</body>
</html>