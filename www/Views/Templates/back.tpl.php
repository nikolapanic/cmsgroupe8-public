<?php
namespace App\Views;
use App\Core\Helpers;
?>
<!DOCTYPE html>
<html lang="FR">
<head>
	<meta charset="UTF-8">
	<title>PoweRent</title>
	<meta name="description" content="ceci est la description de ma page">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<link rel="stylesheet" href="../../public/css/main.css">
</head>
<body>
	<header class="header admin">
		<div class="container-fluid">
			<div class="flex">
				<button class="mobile-menu-btn">
					<div></div>
					<div></div>
					<div></div>
				</button>
				<a href="/">
					<img class="logo" src="../../public/images/logo.png" alt="Logo PowerRent">
				</a>
				<div class="line"></div>
			</div>
			<div class="flex">
				<img class="user-img" src="../../public/images/<?= Helpers::checkPhotoExist($_SESSION['profile_picture'])?>" alt="Logo admin">
				<a href="/settings">
					<span class="iconify mobile-settings" data-icon="akar-icons:gear" data-inline="false"></span>
				</a>
			</div>
		</div>
	</header>

	<!-- intégration de la vue -->
	<main class="flex">
		<nav class="menu">
			<div class="close-btn">
				<button>
					X
				</button>
			</div>
			<ul>
				<li class="menu-link">
					<a href="/">
						<span class="iconify item" data-icon="fa-home"></span>
						<p class="item">Home</p>
					</a>
				</li>

				<li class="menu-link">
					<a href="/apparence">
						<span class="iconify item" data-icon="cil:color-palette" data-inline="false"></span>
						<p class="item">Theme</p>
					</a>
				</li>

				<li class="menu-link">
					<a href="/pages">
						<span class="iconify item" data-icon="fluent:document-one-page-24-regular" data-inline="false"></span>
						<p class="item">Pages</p>
					</a>
				</li>

				<li class="menu-link">
					<a onclick="submenu()">
						<span class="iconify item" data-icon="grommet-icons:article" data-inline="false"></span>
						<p class="item" >Articles</p>
						<span id="arrow-submenu" class="iconify" data-inline="false" data-icon="dashicons:arrow-down-alt2"></span>
					</a>
				</li>
				<li class="sub-menu-link" id="sub-menu">
				<a href="/articles">
						<span class="iconify" data-inline="false" data-icon="bi:list-ol"></span>
						<p class="item" >Liste des articles</p>
					</a>
					<a href="/create_articles">
						<span class="iconify icon-plus" data-icon="bi:plus-circle-fill" data-inline="false"></span>
						<p class="item" >Créer un Article</p>
					</a>
				</li>
				

				<li class="menu-link">
					<a href="/user">
						<span class="iconify item" data-icon="ant-design:user-outlined" data-inline="false"></span>
						<p class="item" >Utilisateur</p>
					</a>
				</li>

				<li class="menu-link">
					<a href="/galerie">
						<span class="iconify item" data-icon="ant-design:picture-outlined" data-inline="false"></span>
						<p class="item" >Galerie</p>
					</a>
				</li>

				<li class="menu-link">
					<a href="/commentaires">
						<span class="iconify item" data-icon="fa-regular:comment-dots" data-inline="false"></span>
						<p class="item">Commentaire</p>
					</a>
				</li>

				<li class="menu-link">
					<a href="#">
						<span class="iconify item" data-icon="ic:baseline-car-rental" data-inline="false"></span>
						<p class="item">Location</p>
					</a>
				</li>
			</ul>
			<div class="setting">
				<div class="line"></div>
				<div class="menu-link">
					<a href="/settings">
						<span class="iconify item" data-icon="akar-icons:gear" data-inline="false"></span>
						<p class="item">Paramètre</p>
					</a>
				</div>
			</div>
		</nav>
		<section class="container" >
			<?php include $this->view ;?>
		</section>
	</main>
	<script src="https://cdn.jsdelivr.net/npm/@iconify/iconify@1.0.7/dist/iconify.min.js"></script>
	<script src="../../public/js/main.js"></script>
	<script src="../../webpack/node_modules/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
  		tinymce.init({
    	selector: '#mytextarea',
		force_br_newlines : true,
  		force_p_newlines : false,
  		forced_root_block : '',
		height: 400
		
  		});
		  
  	</script>
</body>
</html>