<?php if (isset($_GET['delete_id']) && is_numeric($_GET['delete_id'])) : ?>
    <div class="delete-user">
        <div class="card-message card-message-error">
            <p>Êtes-vous sûr de vouloir supprimer ce fichier ?</p>
            <div class="bouton">
                <?php App\Core\FormBuilder::render($form_delete_articles) ?>
                <a href="/galerie"><button>Non</button></a>
            </div>
        </div>
    </div>

<?php endif; ?>
<div class="row">
    <div class="col-11">
        <h1>Articles</h1>
    </div>
</div>
<div class="row head-container">
    <div class="col-3 results">
        <h3><?= $nb_articles ?> résultats</h3>
    </div>
    <div class="col-8 head-container-btn">
        <a href="/create_articles">
            <button class="button-secondary">
                <span class="iconify plus" data-icon="akar-icons:plus" data-inline="false"></span>
                <p> Ajouter un article</p>
            </button></a>
        <div class="custom-select">
            <select name="filter" id="">
                <option value="default" selected>Filtre</option>
                <option value="nom">nom</option>
                <option value="date">date</option>
            </select>
            <span class="iconify" data-icon="bi:filter" data-inline="false"></span>
        </div>
    </div>
</div>
<div class="row row-articles">
    <?php if ($nb_articles < 1) : ?>
        <p>Aucun article pour le moment</p>
    <?php else : ?>
        <?php foreach ($article as $articles) : ?>
            <div class="col-5 col-lg-5">
                <div class="col-inner utilisateur">
                    <div class="flex">
                        <!-- <img class="user-img" src="../public/asset/todoroki.jpg" alt="user"> -->
                        <div class="info">

                            <h2><?= $articles["title"]; ?></h2>
                            <h3>Catégorie : <?= $articles["categorie"]; ?></h3>
                            <p>Auteur: <?= $articles["author"]; ?></p>
                            <p>Date de création: <?= $articles["date"]; ?></p>
                        </div>
                    </div>
                    <div>
                        <div class="icon-btn icon-btn-articles">
                            <button>
                                <a href="/modify_articles/<?= $articles["id"]; ?>">
                                    <span class="iconify modifier" data-inline="false" data-icon="ant-design:edit-outlined"></span>
                                </a>
                            </button>
                            <button>
                                <a href="<?= $_SERVER['REQUEST_URI'] ?>?delete_id=<?= $articles['id'] ?>">
                                    <span class="iconify delete" data-inline="false" data-icon="carbon:delete"></span>
                                </a>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        <?php endforeach; ?>
    <?php endif; ?>

</div>
<div class="row add-btn">
    <div class="col-11">
        <div class="line"></div>
    </div>
    <a href="/create_articles">
        <button>
            <span class="iconify icon-plus" data-icon="bi:plus-circle-fill" data-inline="false"></span>
        </button></a>
</div>
<?php if (isset($success)) : ?>
    <div class="card-message card-message-success">
        <p><?= $success ?></p>
        <button id="close-message-button">X</button>
    </div>
<?php endif; ?>