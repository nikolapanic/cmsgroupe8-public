<div class="create_user">
	<h1>Créer un utilisateur</h1>

	<?php if(isset($errors)):?>
	<div class="card-message card-message-error">
		<p>
			<?php foreach ($errors as $error):?>
				<?=$error;?><br/>
			<?php endforeach;?>
		</p>
		<button id="close-message-button">X</button>
	</div>
	<?php endif;?>
	<?php if(isset($success)):?>
	<div class="card-message card-message-success">
		<p><?=$success?></p>
		<button id="close-message-button">X</button>
	</div>
	<?php endif;?>


	<?php App\Core\FormBuilder::render($form)?>
</div>


