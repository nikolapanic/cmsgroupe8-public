<section class="container-fluid">
    <div class="row row-connexion">
        <div class="col-5 content">
            <div class="col-inner">
                <h1 id="title">Power rent</h1>
                <h2 id="description">Power Rent la référence pour vos site de <br/> gestion et location de véhicules en un clic </h2>
                <h4>Connexion &#128640;</h4>

                <?php if(isset($errors)):?>
                
                <div class="card-message card-message-error">
                <p>
                    <?php foreach ($errors as $error):?>
                        <?=$error;?><br/>
                    <?php endforeach;?>
                </p>
                <button id="close-message-button">X</button>
                </div>
                <?php endif;?>

                <?php if(isset($success)):?>
                
                <div class="card-message card-message-success">
                <p>
                        <?=$success;?><br/>
                </p>
                <button id="close-message-button">X</button>
                </div>
                <?php endif;?>

                <?php App\Core\FormBuilder::render($formLogin)?>
                <?php if($user_exist<1):?>
                <a href="/s-inscrire">Pas encore de compte ?</a>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>