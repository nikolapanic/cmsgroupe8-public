
<div onclick="close_preview()" class="bg-preview" id="bg_preview"></div>
    <div class="preview-articles" id="preview_articles">
        <span onclick="close_preview()" class="close_button">X</span>
        <div id="title_preview"></div>
        <div id="preview_content"></div>
    </div>
    
<form method="POST">
<h1 id="title">Créer un article</h1>
<section id="create_articles">
<div class="left-block">
    <?php App\Core\FormBuilder::render($form)?>
</div>
    <!-- <div class="left-block">
        <div class="title">
            <p>Nom de l'article</p>
            <input id="name_article" name="title" type="text" required="required">
        </div>
        <div class="content">
            <p style="margin-bottom:10px;">Contenu</p>
            <textarea id="mytextarea" name="Content" required="required"></textarea>
        </div>
    </div> -->
    <div class="right-block">
    <span class="preview">
            <a id=btn-preview onclick="preview()"><span class="iconify" data-inline="false" data-icon="bi:eye-fill" style="color: #000; margin-left:5px; margin-right:10px; width:18px;"></span>Preview</a>
        </span>
        <div class="author">
            <p>Auteur</p>
            <select name="author" id="author-select" required>
                <?php foreach ($data as $users):?>
                    <option><?= $users["firstname"];?> <?= $users["lastname"]; ?></option>
                <?php endforeach;?>
            </select>
        </div>
        
        <div name="categorie" class="categorie">
            <label for="categorie">Catégorie :</label>
            <input list="categorie" name="categorie" id="list_categorie" required>
            <datalist id="categorie" id="add_categorie">
                <?php foreach ($cat as $categorie):?>
                    <option><?= $categorie["categorie"];?></option>
                <?php endforeach;?>
            </datalist>
            
        </div>
        
        <div class="status">
            <p>Etat</p>
            <div class="states">
                <p>Modifié le : </p>
                <p>Publié le : </p>
            </div>
        </div>
        <div class="btn-publish-delete">
            <span class="submit-btn">
                <button type="submit" id="btn-submit">Publier</button>
            </span>
            <span class="delete-btn">
                <button id="btn-delete" onclick="window.location.href='/'">Supprimer</button>
            </span>
        </div>
    </div>

    </form>
    
</section>


