<div class="row">
    <div class="col-11">
        <h1>Theme</h1>
    </div>
</div>

<?php if(isset($errors)):?>
<div class="card-message card-message-error">
    <p>
        <?php foreach ($errors as $error):?>
            <?=$error;?><br/>
        <?php endforeach;?>
    </p>
    <button id="close-message-button">X</button>
</div>
<?php endif;?>
<?php if(isset($success)):?>
<div class="card-message card-message-success">
    <p><?=$success?></p>
    <button id="close-message-button">X</button>
</div>
<?php endif;?>

<div class="row head-container">
    <div class="col-3 results">
        <?php if(isset($nb_articles) && !empty($nb_articles)):?>
            <h3><?= $nb_articles ?> résultats</h3>
            <?php else: ?>
            <h3>0 résultat</h3>
            <?php endif ?>
        </div>

    <div class="col-8 head-container-btn">
        
        <div class="custom-select">
            <select name="filter" id="">
                <option value="default" selected>Filtre</option>
                <option value="nom">nom</option>
                <option value="date">date</option>
            </select>
            <span class="iconify" data-icon="bi:filter" data-inline="false"></span>
        </div>
    </div>
</div>

<div class="row row-card theme">
<?php if(isset($theme) && !empty($theme)):?>
<?php foreach ($theme as $themes):?>
    <div class="col-5 col-lg-5">
        <div class="col-inner card">
            <div class="flex header-card">
                <h2><?= $themes["nom"];?></h2>
                <?php if ($themes["isSet"] == 1):?>
                    <span id="actif" class="iconify" data-inline="false" data-icon="akar-icons:check"></span>
                <?php else :?>
                    <span id="delete" class="iconify delete" data-inline="false" data-icon="carbon:delete"></span>
                <?php endif;?>        
            </div>
            <div>
                <img class="image-preview" src="<?= $themes["chemin"];?>" alt="theme">
                <?php if ($themes["isSet"] == 1):?>
                    <button class="theme_btn theme_btn_active">Désactiver</button>
                <?php else :?>
                    <button class="theme_btn">Activer</button>
                <?php endif;?>
            </div>
        </div>
    </div>
<?php endforeach;?>
<?php else: ?>
    <p>AUCUN THEME EXISTANT</p>
<?php endif?>
    
</div>

