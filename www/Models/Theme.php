<?php

namespace App\Models;

use App\Core\Database;
use App\Core\Helpers;

class Theme extends Database
{

	private $id = null;
	protected $nom;
	protected $is_set;
	protected $chemin;

	public function __construct(){
		parent::__construct();
	}

	/**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
        // double action de peupler l'objet avec ce qu'il y a en bdd
        // https://www.php.net/manual/fr/pdostatement.fetch.php
    }

    /**
     * @return mixed
     */
    public function getIsSet()
    {
        return $this->is_set;
    }

    /**
     * @param mixed $is_set
     */
    public function setIsSet($is_set)
    {
        $this->is_set = $is_set;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getChemin(): string
    {
        return $this->chemin;
    }

    /**
     * @param mixed $chemin
     */
    public function setContent($chemin)
    {
        $this->content = $chemin;
    }

    public function boot($pdo,$table){
        $query = $pdo->exec("CREATE TABLE `$table` (
            `id` int(11) NOT NULL,
            `nom` varchar(40) NOT NULL,
            `isSet` int(11) NOT NULL,
            `chemin` varchar(255) NOT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
          INSERT INTO `$table` (`id`, `nom`, `isSet`, `chemin`) VALUES
          (1, 'basic', 1, '../public/images/theme_1.jpg'),
          (2, 'retro', 0, '../public/images/theme_2.jpg');
          ALTER TABLE `$table`
            ADD PRIMARY KEY (`id`);
        ALTER TABLE `$table`
        MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
        COMMIT;");
    }
    
}
