<?php

namespace App\Models;

use App\Core\Database;
use App\Core\Helpers;
use App\Core\TokenCreator as Token;

class User extends Database
{

	private $id = null;
	protected $firstname;
	protected $lastname;
	protected $email;
	protected $pwd;
	protected $role;
	protected $status = 0;
	protected $isDeleted = 0;
	protected $photo;

	public function __construct(){
		parent::__construct();
	}

	/**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
        // double action de peupler l'objet avec ce qu'il y a en bdd
        // https://www.php.net/manual/fr/pdostatement.fetch.php
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPwd()
    {
        return $this->pwd;
    }

    /**
     * @param mixed $pwd
     */
    public function setPwd($pwd)
    {
        $this->pwd = $pwd;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getIsDeleted(): int
    {
        return $this->isDeleted;
    }

    /**
     * @param int $idDeleted
     */
    public function setIsDeleted(int $isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role)
    {
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getPhoto(): string
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     */
    public function setPhoto(string $photo)
    {
        $this->photo = $photo;
    }
    
    public function boot($pdo,$table){
        $query = $pdo->exec("CREATE TABLE IF NOT EXISTS $table (
            `id` int(11) NOT NULL,
            `firstname` varchar(55) NOT NULL,
            `lastname` varchar(255) NOT NULL,
            `email` varchar(320) NOT NULL,
            `pwd` varchar(250) NOT NULL,
            `role` varchar(14) NOT NULL DEFAULT 'administrateur',
            `status` tinyint(4) NOT NULL DEFAULT '0',
            `photo` varchar(256) DEFAULT NULL,
            `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
            `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updatedAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
          ALTER TABLE `$table`
          ADD PRIMARY KEY (`id`);
          ALTER TABLE `$table` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
          ALTER TABLE `$table`
            MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;");
    }

    public function deleteUser($token,$id){
        $form =[
            "config"=>[
            "method"=>"POST",
            "action"=>"",
            "class"=>"delete",
            'id'=> 'delete_user',
            "submit"=>"Oui"
            ],
            "inputs"=>[
                "id"=>[
                    "type"=>"hidden",
                    "name"=> "id",
                    "value"=>$id
                ],
                "token"=>[
                    "type"=>"hidden",
                    "value"=> $token
                ],
                "isDeleted"=>[
                    "type"=>"hidden",
                    "value"=> 1
                ]
            ],
        ];
        return $form;
        
    }

    public function formModifUser($token){
        $form = [
            "config"=>[
            "id" => "form_modification_user",
            "method"=>"POST",
            "action"=>"",
            "class"=>"form_builder form_modification_user",
            "submit"=>"Validation"
            ],
            "inputs"=>[
                "id"=>[
                    "type"=>"hidden",
                    "name"=> "id"
                ],
                "lastname"=>[ 
                    "type"=>"text",
                    "label"=>"Nom",
                    "minLength"=>2,
                    "maxLength"=>255,
                    "class"=>"form_input lastname",
                    "error"=>"Votre nom doit faire entre 2 et 255 caractères",
                    "required"=>true
                ],
                "firstname"=>[ 
                    "type"=>"text",
                    "label"=>"Prénom",
                    "minLength"=>2,
                    "maxLength"=>55,
                    "class"=>"form_input firstname",
                    "error"=>"Votre prénom doit faire entre 2 et 55 caractères",
                    "required"=>true
                ],
                
                "email"=>[ 
                    "type"=>"email",
                    "label"=>"Email",
                    "minLength"=>8,
                    "maxLength"=>320,
                    "class"=>"form_input email",
                    "error"=>"Votre email doit faire entre 8 et 320 caractères",
                    "required"=>true
                ],
                "role"=>[ 
                    "type"=>"radio",
                    "class"=>"form_input",
                    "label"=>[
                        "Administrateur",
                        "Editeur"
                    ],
                ],
                "token"=>[
                    "type"=>"hidden",
                    "value"=> $token
                ],
            ]
        ];
        
        return $form;
    }
    public function formRegister($token){
        // TODO : faire un select au lieu d'insérer l'image
        $path = Helpers::getPathname();
        $form = [
            "config"=>[
                "method"=>"POST",
                "action"=>"",
                'enctype'=>"",
                "id"=>"form_register",
                "class"=>"form_builder form_create_user",
                "submit"=>"Inscription"  
            ],
            "inputs"=>[
                "image"=>[
                    "type"=>'file',
                    "label"=>'Photo de profil',
                    'class'=>'form_input',
                    'id'=>'image'
                ],
                "lastname"=>[ 
                    "type"=>"text",
                    "label"=>"Nom",
                    "minLength"=>2,
                    "maxLength"=>255,
                    "id"=>"lastname",
                    "class"=>"form_input",
                    "placeholder"=>"Ex: Johnson",
                    "error"=>"Votre nom doit faire entre 2 et 255 caractères",
                    "required"=>true
                ],
                "firstname"=>[ 
                    "type"=>"text",
                    "label"=>"Prénom",
                    "minLength"=>2,
                    "maxLength"=>55,
                    "id"=>"firstname",
                    "class"=>"form_input",
                    "placeholder"=>"Ex: Dwayne",
                    "error"=>"Votre prénom doit faire entre 2 et 55 caractères",
                    "required"=>true
                ],
                "email"=>[ 
                    "type"=>"email",
                    "label"=>"Email",
                    "minLength"=>8,
                    "maxLength"=>320,
                    "id"=>"email",
                    "class"=>"form_input",
                    "placeholder"=>"Ex: nom@gmail.com",
                    "error"=>"Votre email doit faire entre 8 et 320 caractères",
                    "required"=>true
                ],
                "pwd"=>[ 
                    "type"=>"password",
                    "label"=>"Mot de passe",
                    "minLength"=>8,
                    "id"=>"pwd",
                    "class"=>"form_input",
                    "placeholder"=>"8 caractères minimum",
                    "error"=>"Votre mot de passe doit faire au minimum 8 caractères",
                    "required"=>true
                ],
                "pwdConfirm"=>[ 
                    "type"=>"password",
                    "label"=>"Confirmation",
                    "confirm"=>"pwd",
                    "id"=>"pwdConfirm",
                    "class"=>"form_input",
                    "placeholder"=>"Encore un petit effort...",
                    "error"=>"Votre mot de mot de passe de confirmation ne correspond pas",
                    "required"=>true
                ],
                "token"=>[
                    "type"=>"hidden",
                    "value"=> $token
                ],
            ]
        ];
        if($path === 'user/create_user'){
            $role = [
                "type"=>"radio",
                "class"=>"form_input",
                "label"=>[
                    "Administrateur"=>"checked",
                    "Editeur"
                ],
            ];
            $form['inputs']['role'] = $role;
        }else{
            $role = [
                "type"=>"hidden",
                "value"=> "Administrateur"
            ];
            $form['inputs']['role'] = $role;
        }
        return $form;
    }


    public function formLogin($token){
        return [
            "config"=>[
                "method"=>"POST",
                "action"=>"",
                "id"=>"form_login",
                "class"=>"form_builder",
                "submit"=>"Se connecter"
            ],
            "inputs"=>[
                "email"=>[ 
                    "type"=>"email",
                    "label"=>"Email",
                    "minLength"=>8,
                    "maxLength"=>320,
                    "id"=>"email",
                    "class"=>"form_input",
                    "placeholder"=>"Exemple: nom@gmail.com",
                    "error"=>"Votre email doit faire entre 8 et 320 caractères",
                    "required"=>true
                ],
                "pwd"=>[ 
                    "type"=>"password",
                    "label"=>"Mot de passe",
                    "minLength"=>8,
                    "id"=>"pwd",
                    "class"=>"form_input",
                    "placeholder"=>"8 caratères minimum",
                    "error"=>"Votre mot de passe doit faire au minimum 8 caractères",
                    "required"=>true
                ],
                "token"=>[
                    "type"=>"hidden",
                    "value"=> $token
                ],
                
            ]

        ];
    }
}
