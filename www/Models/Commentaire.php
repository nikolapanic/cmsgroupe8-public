<?php
namespace App\Models;

use App\Core\Database;
use App\Core\Helpers;

class Commentaire extends Database
{
	private $id = null;
	protected $author;
	protected $date;
    protected $content;
    protected $path;

    public function __construct(){
		parent::__construct();
	}

	/**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
        // double action de peupler l'objet avec ce qu'il y a en bdd
        // https://www.php.net/manual/fr/pdostatement.fetch.php
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    public function boot($pdo,$table){
        $query = $pdo->exec("CREATE TABLE `$table` (
            `id` int(11) NOT NULL,
            `author` varchar(255) NOT NULL,
            `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `content` varchar(255) NOT NULL,
            `path` varchar(50) NOT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
          ALTER TABLE `$table`
            ADD PRIMARY KEY (`id`);
          ALTER TABLE `$table`
            MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
          COMMIT;");
    }

}