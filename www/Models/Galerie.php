<?php
namespace App\Models;

use App\Core\Database;
use App\Core\Helpers;

class Galerie extends Database
{
    private $id = null;
	protected $author;
    protected $filename;
    protected $profile_picture;

    public function __construct(){
		parent::__construct();
	}

	/**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
        // double action de peupler l'objet avec ce qu'il y a en bdd
        // https://www.php.net/manual/fr/pdostatement.fetch.php
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->filename;
    }

    /**
     * @param mixed $file_name
     */
    public function setFileName($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return mixed
     */
    public function getProfilePicture()
    {
        return $this->profile_picture;
    }

    /**
     * @param mixed $file_name
     */
    public function setProfilePicture($profile_picture)
    {
        $this->profile_picture = $profile_picture;
    }

    public function boot($pdo,$table){
        $query = $pdo->exec("CREATE TABLE IF NOT EXISTS $table (
            `id` int(11) NOT NULL,
            `author` varchar(256) NOT NULL,
            `filename` varchar(256) NOT NULL,
            `profile_picture` tinyint(1) NOT NULL DEFAULT '0',
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
          ALTER TABLE `$table`
          ADD PRIMARY KEY (`id`);
          ALTER TABLE `$table`
            MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;");
    }

    public function addImg($token){
        $form =[
            "config"=>[
                "method"=>"POST",
                "action"=>"",
                'enctype'=>"",
                "id"=>"form_add_img",
                "class"=>"form_builder form_add_img form_create_user",
                "submit"=>"Ajouter"  
            ],
            "inputs"=>[
                "image"=>[
                    "type"=>'file',
                    "label"=>'Image',
                    'class'=>'form_input',
                    'id'=>'image'
                ],
                "token"=>[
                    "type"=>"hidden",
                    "value"=> $token
                ],
            ]
        ];
            
              
        
        return $form;
    }

    public function deleteImg($token,$id){
        $form =[
            "config"=>[
            "method"=>"POST",
            "action"=>"",
            "class"=>"delete",
            'id'=> 'delete_img',
            "submit"=>"Oui"
            ],
            "inputs"=>[
                "id"=>[
                    "type"=>"hidden",
                    "name"=> "id",
                    "value"=>$id
                ],
                "token"=>[
                    "type"=>"hidden",
                    "value"=> $token
                ]
            ],
        ];
        return $form;
        
    }
}