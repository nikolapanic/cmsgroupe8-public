<?php

namespace App\Models;

use App\Core\Database;
use App\Core\Helpers;

class Articles extends Database
{

	private $id = null;
	protected $author;
	protected $date;
	protected $categorie;
	protected $title;
	protected $content;

	public function __construct(){
		parent::__construct();
	}

	/**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
        // double action de peupler l'objet avec ce qu'il y a en bdd
        // https://www.php.net/manual/fr/pdostatement.fetch.php
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getContent(): int
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }


    public function boot($pdo,$table){
        // boot la table article
        $query = $pdo->exec("CREATE TABLE `$table` (
            `id` int(11) NOT NULL,
            `title` varchar(70) DEFAULT NULL,
            `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `author` varchar(30) DEFAULT NULL,
            `categorie` varchar(40) DEFAULT NULL,
            `Content` text CHARACTER SET utf8 COLLATE utf8_bin
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ALTER TABLE `$table`
            ADD PRIMARY KEY (`id`);
            ALTER TABLE `$table` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
            ALTER TABLE `$table`
            MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
            COMMIT;");
    }

    public function formAddArticles($token){
        return [
            "config"=>[
                "method"=>"POST",
                "action"=>"",
                "id"=>"form_articles",
                "class"=>"form_builder",
                "submit"=>"Créer"
            ],
            "inputs"=>[
                "title"=>[ 
                    "type"=>"text",
                    "label"=>"Titre",
                    "minLength"=>6,
                    "maxLength"=>80,
                    "id"=>"name_article",
                    "class"=>"form_input",
                    "required"=> true
                ],
                
                "Content"=>[ 
                    "type"=>"",
                    "label"=>"Contenu",
                    "id"=>"mytextarea",
                    "class"=>"form_articles",
                    "placeholder"=>"Contenu de l'article"
                ],
                "token"=>[
                    "type"=>"hidden",
                    "value"=> $token
                ],
            
            ],

        ];
    }

    public function deleteArticles($token,$id){
        $form =[
            "config"=>[
            "method"=>"POST",
            "action"=>"",
            "class"=>"delete",
            'id'=> 'delete_article',
            "submit"=>"Oui"
            ],
            "inputs"=>[
                "id"=>[
                    "type"=>"hidden",
                    "name"=> "id",
                    "value"=>$id
                ],
                "token"=>[
                    "type"=>"hidden",
                    "value"=> $token
                ]
            ],
        ];
        return $form;
        
    }

   
}


 