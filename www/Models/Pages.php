<?php

namespace App\Models;

use App\Core\Database;
use App\Core\Helpers;

class Pages extends Database
{

	private $id = null;
	protected $author;
	protected $date;
	// protected $categorie;
	protected $title;
	protected $content;

	public function __construct(){
		parent::__construct();
	}

	/**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
        // double action de peupler l'objet avec ce qu'il y a en bdd
        // https://www.php.net/manual/fr/pdostatement.fetch.php
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getContent(): int
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    public function boot($pdo,$table){
        // boot la table article
        $query = $pdo->exec("CREATE TABLE `$table` (
            `id` int(11) NOT NULL,
            `title` varchar(70) DEFAULT NULL,
            `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `author` varchar(30) DEFAULT NULL,
            `Content` varchar(800) DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ALTER TABLE `$table`
            ADD PRIMARY KEY (`id`);
            ALTER TABLE `$table` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
            ALTER TABLE `$table`
            MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
            COMMIT;");
    }

    

    public function formAddPage($token){
        return [
            "config"=>[
                "method"=>"POST",
                "action"=>"",
                "id"=>"form_articles",
                "class"=>"form_builder",
                "submit"=>"Créer"
            ],
            "inputs"=>[
                "Titre"=>[ 
                    "type"=>"text",
                    "label"=>"Titre",
                    "minLength"=>6,
                    "maxLength"=>80,
                    "id"=>"name_article",
                    "class"=>"form_input",
                    "required"=>true
                ],
                "Content"=>[ 
                    "type"=>"",
                    "label"=>"Contenu",
                    "id"=>"mytextarea",
                    "class"=>"form_articles",
                    "placeholder"=>"Contenu de l'article"
                ],
                "token"=>[
                    "type"=>"hidden",
                    "value"=> $token
                ],
            
            ],

        ];
    }

    public function formDeletePage($token){
        $form =[
            "config"=>[
                "method"=>"POST",
                "action"=>"",
                "id"=>"delete_user",
                "class"=>"form_builder",
                "submit"=>"Oui"
            ],
            "inputs"=>[
                "token"=>[
                    "type"=>"hidden",
                    "value"=> $token
                ],
            ]

        ];
        return $form;
    }
}

