<?php

namespace App;

use App\Controller\Installer;
use App\Core\Router;
use App\Core\Helpers as h;
use App\Core\ConstantMaker;

require "Autoload.php";

Autoload::register();

// $uri  => /se-connecter?user_id=2 => /se-connecter
if(!file_exists('.env')){
	include "./Controllers/Installer.php";
	$install = new Installer();
	$install->showInstallerAction();

	$envdata = $install->getEnvData();
	if(!empty($envdata)){
		$answer = $install->createEnvFiles($envdata);
		if(isset($answer['success'])){
			new ConstantMaker();
			$response = $install->createDb();
			if(isset($response['success'])){
				$_SESSION["message"]= $response['success'];
				header('Location: /login');
			}
		}
	}
}else{
new ConstantMaker();

$uriExploded = explode("?", $_SERVER["REQUEST_URI"]);

$uri = $uriExploded[0];

$router = new Router($uri);

$c = $router->getController();
$a = $router->getAction();

//check si une page en bbd exist dans un middleware
if( file_exists("./Controllers/".$c.".php")){

	include "./Controllers/".$c.".php";
	// SecurityController =>  App\Controller\SecurityController

	$c = "App\\Controller\\".$c;
	if(class_exists($c)){
		// $controller ====> SecurityController
		$cObjet = new $c();
		if(method_exists($cObjet, $a)){
			$cObjet->$a();

		}else{
			die("L'action' : ".$a." n'existe pas");
		}

	}else{
	
		die("La classe controller : ".$c." n'existe pas");
	}


}else{
	die("Le fichier controller : ".$c." n'existe pas");
}

}